package com.example.drinkwater.controller;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;


import java.util.Dictionary;
import java.util.Hashtable;
@RunWith(AndroidJUnit4.class)
public class DrinkWaterActivityControllerTest  {
    @Test
    public void addDrink(){
        DrinkWaterActivityController controller = new  DrinkWaterActivityController();
        Dictionary data = new Hashtable();
        String ans = "Success";
        data.put("uid", "midOilIsStupid");
        data.put("waterVolume","250" );
        Dictionary result = controller.addDrink(data);
        assertEquals(ans,result.get("result").toString());
    }
}