package com.example.drinkwater.controller;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.Dictionary;
import java.util.Hashtable;

public class MainActivityControllerTest {

    @Test
    public void GetTotalTotalWater() {
        MainActivityController controller  = new MainActivityController();
        Dictionary dict_uid = new Hashtable();
        dict_uid.put("uid","midOilIsStupid");
        Dictionary result = controller.getTotalTotalWater(dict_uid);
        assertEquals("2500",result.get("result").toString());
    }
    @Test
    public void GetDailyDrinkAmount() {
        MainActivityController controller  = new MainActivityController();
        Dictionary dict_uid = new Hashtable();
        dict_uid.put("uid","midOilIsStupid");
        Dictionary result = controller.getDailyDrinkAmount(dict_uid);
        assertEquals("2500",result.get("result").toString());
    }
}