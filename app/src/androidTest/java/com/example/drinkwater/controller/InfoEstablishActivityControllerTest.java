package com.example.drinkwater.controller;



import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

import java.util.Dictionary;
import java.util.Hashtable;

public class InfoEstablishActivityControllerTest  {
    @Test
    public void AddUser() {
        InfoEstablishActivityController controller =  new InfoEstablishActivityController();
        String ans = "Success";
        Dictionary data = new Hashtable();
        data.put("name", "Jack");
        data.put("age", "18");
        data.put("height", "170");
        data.put("weight", "80");
        data.put("totalcc", "2500");
        data.put("uid", "midOilIsStupid");
        Dictionary result = controller.addUser(data);
        assertEquals(ans,result.get("result").toString());
    }
}