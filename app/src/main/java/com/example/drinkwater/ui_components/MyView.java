
package com.example.drinkwater.ui_components;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.example.drinkwater.R;

public class MyView extends View {

    public MyView(Context context) {
        super(context);
        init();
    }

    public MyView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public MyView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    //波浪、圓圈、文字畫筆
    private Paint wavePaint = null;
    private Paint backgroundPaint = null;
    private Paint circlePaint = null;
    private Paint circlePaint_in = null;
    private Paint circlePaint_out = null;
    private Paint textPaint = null;

    //控件寬、高
    private int screenWidth = 0;
    private int screenHeight= 0;

    //震幅
    private final int amplitude = 50;
    private Path path = null;
    private float ychange = 25f; //水波上下擺動
    private float ychange_step = 0.25f; //水波上下擺動更改幅度

    //進度
    private int progress = 0;
    private int remaining = 0;
    private boolean colorchange = true;

    //起始點
    private Point startPoint = new Point();

    public void setProgress(int totalCC, int drinkCC, boolean colorchange) {
        progress = drinkCC * 100 / totalCC;
        if (progress == 100) {
            this.progress = progress + amplitude;
        }
        remaining = totalCC - drinkCC;
        this.colorchange = colorchange;
    }

    private void init() {
        wavePaint = new Paint();
        wavePaint.setAntiAlias(true);

        textPaint = new Paint();
        textPaint.setAntiAlias(true);
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize(100f);

        circlePaint = new Paint();
        circlePaint.setAntiAlias(true);
        circlePaint.setColor(getResources().getColor(R.color.blue_normal));
        circlePaint.setStyle(Paint.Style.FILL);

        backgroundPaint = new Paint();
        backgroundPaint.setAntiAlias(true);
        backgroundPaint.setColor(getResources().getColor(R.color.white_background));
        backgroundPaint.setStyle(Paint.Style.FILL);

        circlePaint_in = new Paint();
        circlePaint_in.setAntiAlias(true);
        circlePaint_in.setStrokeWidth(30f);
        circlePaint_in.setColor(getResources().getColor(R.color.white_background));
        circlePaint_in.setStyle(Paint.Style.STROKE);

        circlePaint_out = new Paint();
        circlePaint_out.setAntiAlias(true);
        circlePaint_out.setStrokeWidth(40f);
        circlePaint_out.setColor(getResources().getColor(R.color.blue_normal));
        circlePaint_out.setStyle(Paint.Style.STROKE);
    }

    //根據xml佈局檔案和程式碼中對控制元件屬性的設定，來獲取或者計算出每個View和ViewGroup的尺寸
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int size = Math.min(measureSize(400, widthMeasureSpec), measureSize(400, heightMeasureSpec));
        setMeasuredDimension(size, size);
    }

    @Override
    protected void onSizeChanged(int w,int h,int oldw,int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        screenWidth = w;
        screenHeight = h;
        startPoint.x = -screenWidth;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //畫圓、畫波浪線、畫進度文字
        drawCircle(canvas);
        drawWave(canvas);
        drawText(canvas);
        drawCircleFrame(canvas);
        postInvalidateDelayed(10);
    }

    //剪裁並畫出一個圓
    private void drawCircle(Canvas canvas) {
        //水波球繪製
        Path circlePath = new Path();
        circlePath.addCircle((float)screenWidth / 2,(float)screenHeight / 2,(float)screenHeight / 2,Path.Direction.CW);
        canvas.clipPath(circlePath);

        //背景繪製
        canvas.drawCircle((float)screenHeight / 2,(float)screenHeight / 2,(float)screenHeight / 2, backgroundPaint);
    }

    private void drawWave(Canvas canvas) {
        int height = (int)((float)progress / 100 * screenHeight);
        startPoint.y = -height + (int)ychange;
        canvas.translate(0f, screenHeight);
        Path path = new Path();

        float wave = (float)screenWidth / 4;
        path.moveTo(startPoint.x, startPoint.y);
        for (int i =0; i < 4; i++) {
            float startX = startPoint.x + i * wave * 2;
            float endX = startX + wave * 2;
            if (i % 2 == 0) {
                path.quadTo(((startX + endX) / 2),(startPoint.y + amplitude),endX,startPoint.y);
            } else {
                path.quadTo(((startX + endX) / 2),(startPoint.y - amplitude),endX,startPoint.y);
            }
        }
        path.lineTo(screenWidth, (float)screenHeight / 2);
        path.lineTo(-screenWidth, (float)screenHeight / 2);
        path.lineTo(-screenWidth, 0f);
        path.close();

        wavePaint.setStyle(Paint.Style.FILL);

        if(colorchange){
            if(progress>25){
                if(progress>50){
                    wavePaint.setColor(getResources().getColor(R.color.blue_normal));
                }else{
                    wavePaint.setColor(getResources().getColor(R.color.orange));
                }
            }else{
                wavePaint.setColor(getResources().getColor(R.color.red));
            }
        }else{
            wavePaint.setColor(getResources().getColor(R.color.blue_normal));
        }

        canvas.drawPath(path, wavePaint);
        startPoint.x += 4;
        if (startPoint.x > 0) startPoint.x = -screenWidth;
        if (ychange == 0 || ychange > 25f) ychange_step *= -1;
        ychange -= ychange_step;
        path.reset();
    }

    //畫出圓的外框
    private void drawCircleFrame(Canvas canvas) {
        //外框線條繪製
        canvas.drawCircle((float)screenHeight / 2,(float)screenHeight / -2,(float)screenHeight / 2 - 20, circlePaint_out);
        canvas.drawCircle((float)screenHeight / 2,(float)screenHeight / -2,(float)screenHeight / 2 - 50, circlePaint_in);
    }

    private void drawText(Canvas canvas) {
        Rect targetRect = new Rect(0, -screenHeight, screenWidth, 0);
        Paint.FontMetricsInt fontMetrics = textPaint.getFontMetricsInt();
        float baseline = (float)(targetRect.bottom + targetRect.top - fontMetrics.bottom - fontMetrics.top) / 2;
        textPaint.setTextAlign(Paint.Align.CENTER);
        //canvas.drawText(textProgress+"%", targetRect.centerX(), baseline, textPaint);
        canvas.drawText("剩餘 " + remaining + "cc", targetRect.centerX(), baseline, textPaint);
    }

    private int measureSize(int defaultSize, int measureSpec){
        int result = defaultSize;
        int mode = MeasureSpec.getMode(measureSpec);
        int size = MeasureSpec.getSize(measureSpec);
        switch(mode) {
            case MeasureSpec.UNSPECIFIED:
                result = defaultSize;
                break;
            case MeasureSpec.AT_MOST:
            case MeasureSpec.EXACTLY:
                result = size;
                break;
        }
        return result;
    }

}















