package com.example.drinkwater.repository;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.drinkwater.model.Record;
import com.example.drinkwater.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

public class UserRepository {

    private static final String TAG = "My";
    private static FirebaseFirestore db;

    // 初始化資料庫
    public UserRepository() {
        // 與FireStore串連
//        this.db = FirebaseFirestore.getInstance();
    }

    // Static 建構子進入點
    static {
        // 初始化 FireStore 物件
        initFireStore();
    }
    private static void initFireStore() {
        if (db == null) {
            db = FirebaseFirestore.getInstance();
        }
    }

    // 上傳喝水紀錄到資料庫。
    public static Dictionary drinkWater(Dictionary dict) {
        Record record = (Record) dict.get("record");

        Dictionary result = new Hashtable();

        // 讀取資料庫
        db.collection("record")
                .document(record.getUid())
                .collection("date")
                .document(record.getFullTime())
                .set(record)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        result.put("result", "success");
                        Log.d(TAG, "DocumentSnapshot successfully written!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        result.put("result", "error");
                        Log.w(TAG, "Error writing document", e);
                    }
                });

        // 將資料回傳
        return result;
    }

    // 得到喝水量
    public static Dictionary getDrinkRecord(Dictionary dict) {
        String uid = dict.get("uid").toString();
        String start = dict.get("start").toString();
        String end = dict.get("end").toString();

        // 讀取資料庫
        Task<QuerySnapshot> task = db.collection("record")
                .document(uid)
                .collection("date")
                .whereGreaterThanOrEqualTo("date", start)
                .whereLessThanOrEqualTo("date", end)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("DB", "Found drink record! " + document.getId() + " => " + document.getData());
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

        // 等待所有資料庫的回傳資料讀取完畢
        while (true) {
            if (task.isComplete()) {
                break;
            }
        }

        // 回傳數值
        Dictionary result = new Hashtable();

        // 將資料回傳
        result.put("task", task);
        return result;
    }

    // 讀取使用者物件
    public static Dictionary getUser(Dictionary dict) {
        String uid = dict.get("uid").toString();

        // 讀取資料庫
        Task<DocumentSnapshot> task = db.collection("user").document(uid).get();

        // 等待資料讀取完畢
        while (true) {
            if (task.isComplete()) {
                break;
            }
        }

        Dictionary result = new Hashtable();
        if (task.getResult().exists()) {
            // 將資料庫的資料轉換成 User 物件
            User user = task.getResult().toObject(User.class);
            // 將資料回傳
            result.put("result", "success");
            result.put("user", user);
        } else {
            result.put("result", "error");
        }

        return result;
    }

    // 新增使用者到資料庫
    public static Dictionary addUser(Dictionary dict) {
        Dictionary result = new Hashtable();
        User user = (User) dict.get("data");

        // 讀取資料庫
        db.collection("user")
                .document(user.getUid())
                .set(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        result.put("result", "success");
                        Log.d(TAG, "DocumentSnapshot successfully written!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        result.put("result", "error");
                        Log.w(TAG, "Error writing document", e);
                    }
                });

        // 將資料回傳
        return result;
    }

    // 讀取喝水資料表
    public static Dictionary getDrinkCollection(Dictionary dict){
        Dictionary result = new Hashtable();
        Log.d(TAG, "getDrinkCollection: Start to get collection.");
        Task<QuerySnapshot> task = db.collectionGroup("date")
                .whereGreaterThanOrEqualTo("date", dict.get("date"))
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
//                                Log.d(TAG, document.getId() + " => " + document.getData());
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
        Log.d(TAG, "getDrinkCollection: Finish read collection.");


        while(true){
            if(task.isComplete()){
                break;
            }
        }

        result.put("task", task);
        Log.d(TAG, "getDrinkCollection: Return collection.");
        return result;
    }
}
