package com.example.drinkwater.model;

import com.google.firebase.Timestamp;

import java.util.Dictionary;

public class Record {


    private String date;
    private String time;
    private int drinkVolume;
    private Timestamp timestamp;
    private String uid;

    public Record() {

    }

    public Record(Dictionary dict) {
        this.date = dict.get("date").toString();
        this.time = dict.get("time").toString();
        this.uid = dict.get("uid").toString();
        this.drinkVolume = Integer.parseInt(dict.get("waterVolume").toString());
        this.timestamp = (Timestamp) dict.get("timestamp");
    }

    public String getUid() {
        return uid;
    }

    public int getDrinkVolume() {
        return drinkVolume;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    // 取得完整日期
    public String getFullTime() {
        return date + time;
    }
}
