package com.example.drinkwater.model;

public class Bottle {
    // 水壺的資訊

    public int capactiy;
    public String uid;

    public Bottle() {

    }

    public Bottle(int capactiy, String uid) {
        // 初始化水壺的容量
        this.capactiy = capactiy;
        // 初始化使用的UID33
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    public int getCapactiy() {
        return capactiy;
    }

}
