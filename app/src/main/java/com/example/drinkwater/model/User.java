package com.example.drinkwater.model;

import java.util.Dictionary;

public class User {
    private float height;
    private float weight;
    private String name;
    private String uid;
    private int age;
    private int dailyDrinkAmount;
    private int drinkVolume;

    public User() {

    }

    public User(float height, float weight, String name, String uid, int age, int dailyDrinkAmount) {
        this.height = height;
        this.weight = weight;
        this.name = name;
        this.uid = uid;
        this.age = age;
        this.dailyDrinkAmount = dailyDrinkAmount;
    }

    public User(Dictionary dict){
        this.height = Float.parseFloat(dict.get("height").toString());
        this.weight = Float.parseFloat(dict.get("weight").toString());
        this.name = (String) dict.get("name").toString();
        this.uid = (String) dict.get("uid").toString();
        this.age = Integer.parseInt(dict.get("age").toString());
        this.dailyDrinkAmount = Integer.parseInt(dict.get("totalcc").toString());
    }

    public String getUid() {
        return uid;
    }

    public float getHeight() {
        return height;
    }

    public float getWeight() {
        return weight;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getDailyDrinkAmount() {
        return dailyDrinkAmount;
    }

    public int getDrinkVolume() {
        return drinkVolume;
    }

    public void setDrinkVolume(int drinkVolume) {
        this.drinkVolume = drinkVolume;
    }
}
