package com.example.drinkwater.controller;

import com.example.drinkwater.service.UserService;

import java.util.Dictionary;

public class LoginActivityController {


//    private UserService userService;


    public LoginActivityController() {
//        userService = new UserService();
    }

    // input:
    //      "uid": "VnbexdwlJRTRwQauIvdVNrXFnSW2"
    // output:
    //      "result": "success"
    //      "userExist": true
    //檢查使用者是否存在於資料庫中
    public Dictionary checkUserExist(Dictionary dict) {
        Dictionary result = UserService.checkUserExist(dict);
        return result;
    }
}
