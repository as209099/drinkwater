package com.example.drinkwater.controller;


import android.util.Log;

import com.example.drinkwater.service.DrinkAnalyzeService;
import com.example.drinkwater.service.RankingService;
import com.example.drinkwater.service.UserService;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Map;

public class MainActivityController {
    private static final String TAG = "My";
    private static Dictionary rankingResult;
//    private UserService userService;

    public MainActivityController() {
//        Dictionary dict_uid = new Hashtable()

//        Dictionary test = new Hashtable();
//        UserRepository.getDrinkCollection(test);
        ThreadTest();
        // do nothing
    }

    //input:
    //
    //output:
    //      "ranking"(Map<String, Integer>):
    //              "SUGaq90IGKbHqhXDzaE2EfzN02P2" : 8039
    //              "midOilIsStupid" : 3250
    public static Dictionary getRankingResult() {
        return rankingResult;
    }

    private void ThreadTest() {
        Log.d(TAG, "ThreadTest: Start Thread.");
        RankingService rankingRunable = new RankingService();
        Thread thread1 = new Thread(rankingRunable);
        thread1.start();
        try {
            thread1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        synchronized (thread1){
//            try {
//                Log.d(TAG, "ThreadTest: Waiting for thread to complete.");
//                thread1.wait();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
        rankingResult = rankingRunable.getResult();
        Map<String, Integer> rankingData = (Map<String, Integer>) rankingResult.get("ranking");
        for (Map.Entry<String, Integer> en : rankingData.entrySet()) {
            Log.d(TAG, "Key = " + en.getKey() + ", Value = " + en.getValue());
        }
        Log.d(TAG, "ThreadTest: " + RankingService.sum);
        Log.d(TAG, "ThreadTest: Finish Thread.");
    }

    //input:
    //      "uid" : "SUGaq90IGKbHqhXDzaE2EfzN02P2"
    //output:
    //      "result" : "success"
    //      "dailyAnalyze"(Dictionary) :
    //              "averageDrinkTime" : 9856
    //              "startDrink" : 35984
    //              "endDrink" :65553
    //              "longestInterval" : 26283
    //              "shortestInterval" : 3286
    //              "result" : "success"
    //      "weeklyAnalyze"(Dictionary) :
    //              "averageDailyDrinkTime" : 5
    //              "averageStartDrink" : 1850
    //              "averageEndDrink" : 65352
    //              "averageDrinkInterval" : 7548
    //              "result" : "success"
    // 取得喝水分析
    public Dictionary getDrinkAnalyze(Dictionary dict) {
        Dictionary result = DrinkAnalyzeService.getDrinkAnalyze(dict);
        return result;
    }

    //input:
    //      "uid" : "VnbexdwlJRTRwQauIvdVNrXFnSW2"
    //      "start" : "20211226"
    //      "end" : "20211126"
    //output:
    //      "total": 340
    // 從資料庫得到今天的總喝水量
    public Dictionary getTotalWater(Dictionary dict) {
        // 把事件傳到到UserService
        Dictionary result = UserService.getTotalWater(dict);

        // 回傳結果
        return result;
    }

    //input:
    //      "uid" : "VnbexdwlJRTRwQauIvdVNrXFnSW2"
    //      "start" : "20211226"
    //      "end" : "20211126"
    //output:
    //      "drinkRecords"(List<Integer>):
    //              0: 200
    //              1: 300
    //              2: 400
    //      "timeRecords"(List<String>):
    //              0: 073000
    //              1: 083542
    //              2: 095410
    // 從資料庫得到今天的總喝水量
    public Dictionary getAllDrinkRecord(Dictionary dict) {
        // 把事件傳到到UserService
        Dictionary result = UserService.getAllDrinkRecord(dict);

        // 回傳結果
        return result;
    }

    //input:
    //      "uid" : "VnbexdwlJRTRwQauIvdVNrXFnSW2"
    //output:
    //      "dailyDrinkAmount": 2400
    // 取得每天建議喝水量
    public Dictionary getDailyDrinkAmount(Dictionary dict) {
        // 把事件傳到到UserService
        Dictionary result = UserService.getDailyDrinkAmount(dict);

        // 回傳結果
        return result;
    }
}
