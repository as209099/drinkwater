package com.example.drinkwater.controller;

import com.example.drinkwater.service.UserService;

import java.util.Dictionary;

public class InfoEstablishActivityController {

//    private UserService userService;

    // 初始化物件
    public InfoEstablishActivityController() {
//        userService = new UserService();
    }


    //input:
    //      "name": "MidOil"
    //      "age": 20
    //      "height": 173.6
    //      "weight": 83.6
    //      "totalcc" : 2508
    //      "uid": "VnbexdwlJRTRwQauIvdVNrXFnSW2"
    //output:
    // 新增使用者
    public Dictionary addUser(Dictionary dict) {
        Dictionary result = UserService.addUser(dict);

        return result;
    }


}
