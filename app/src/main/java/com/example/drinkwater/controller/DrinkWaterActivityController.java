package com.example.drinkwater.controller;


import com.example.drinkwater.service.UserService;

import java.util.Dictionary;

public class DrinkWaterActivityController {

//    private UserService userService;

    public DrinkWaterActivityController() {
//        userService = new UserService();
    }

    // input:
    //      "uid": "VnbexdwlJRTRwQauIvdVNrXFnSW2"
    //      "waterVolume" : 200
    // output:
    //      "result": "success"
    // 處理喝水事件
    public Dictionary addDrink(Dictionary dict) {
        // 把事件傳到到UserService
        Dictionary result = UserService.drinkWater(dict);
        return result;
    }
}
