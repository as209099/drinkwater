package com.example.drinkwater.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.example.drinkwater.R;
import com.example.drinkwater.controller.InfoEstablishActivityController;

import java.util.Dictionary;
import java.util.Hashtable;

public class InfoEstablishActivity extends AppCompatActivity {

    private InfoEstablishActivityController controller;
    private EditText editText_name;
    private EditText editText_age;
    private EditText editText_height;
    private EditText editText_weight;
    private EditText editText_totalCC;

    private String userId;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infoestablish);

        Intent intent = this.getIntent();
        userId = intent.getStringExtra("uid");

        controller = new InfoEstablishActivityController();

        editText_name = findViewById(R.id.editText_username);
        editText_age = findViewById(R.id.editText_userAge);
        editText_height = findViewById(R.id.editText_userHeight);
        editText_weight = findViewById(R.id.editText_userWeight);
        editText_totalCC = findViewById(R.id.editText_totalCC);

        editText_weight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // do nothing
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // do nothing
            }

            @Override
            public void afterTextChanged(Editable editable) {
                float dailyDrink = Float.valueOf(editText_weight.getText().toString()) * 30;
                editText_totalCC.setText(String.format("%.0f", dailyDrink));
            }
        });

    }

    // 派發事件
    public void eventListener(View view) {
        if (view.getId() == R.id.btn_establish) {
            addUser();
        }
    }

    // 新增使用者
    private void addUser() {
        if (checkField()) {
            Dictionary data = new Hashtable();
            data.put("name", editText_name.getText().toString());
            data.put("age", Integer.valueOf(editText_age.getText().toString()));
            data.put("height", Float.valueOf(editText_height.getText().toString()));
            data.put("weight", Float.valueOf(editText_weight.getText().toString()));
            data.put("totalcc", Integer.valueOf(editText_totalCC.getText().toString()));
            data.put("uid", userId);
            controller.addUser(data);
            switchToMainPage();
        } else {
            Toast.makeText(getApplicationContext(), "請填妥資料以利程式了解您的需求", Toast.LENGTH_LONG).show();
        }
    }

    // 檢查欄位是否都有填入
    private Boolean checkField() {
        String[] field = {
                editText_name.getText().toString(),
                editText_age.getText().toString(),
                editText_height.getText().toString(),
                editText_weight.getText().toString(),
                editText_totalCC.getText().toString(),
        };
        for (String text : field) {
            if (text == "") {
                // 如果有欄位是空白，就回傳 false
                return false;
            }
        }

        return true;
    }

    // 切換畫面
    private void switchToMainPage() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("uid", userId);
        startActivity(intent);
        this.finish();
    }

    @Override
    public void onBackPressed() {
    } //禁用返回鍵
}
