package com.example.drinkwater.activity;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.drinkwater.controller.DrinkWaterActivityController;
import com.example.drinkwater.ui_components.MyView;
import com.example.drinkwater.R;

import java.util.Dictionary;
import java.util.Hashtable;

public class DrinkWaterActivity extends AppCompatActivity {

    private DrinkWaterActivityController controller;

    private MyView my_view;
    private SeekBar seek_bar;
    private EditText add_water_num;
    private int totalCC;
    private int drinkCC;
    private float accurateProgress;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drinkwater);

        //漸層
        ConstraintLayout constraintLayout = findViewById(R.id.background);
        AnimationDrawable animationDrawable = (AnimationDrawable) constraintLayout.getBackground();
        animationDrawable.setEnterFadeDuration(1500);
        animationDrawable.setExitFadeDuration(3000);
        animationDrawable.start();

        Intent intent = this.getIntent();
        userId = intent.getStringExtra("uid");
        drinkCC = Integer.parseInt(intent.getStringExtra("drinkcc"));
        totalCC = Integer.parseInt(intent.getStringExtra("totalcc"))-drinkCC;

        controller = new DrinkWaterActivityController();

        initViews();
    }

    // 派發事件
    public void eventListener(View view) {
        switch (view.getId()) {
            case R.id.btn_add:
                addDrink();
            default:
                switchToMainPage();
                break;
        }
    }

    // 觸發喝水事件
    public void addDrink(){

        Dictionary data = new Hashtable();
        data.put("uid", userId);
        data.put("waterVolume", Integer.parseInt(add_water_num.getText().toString()));

        Dictionary result = this.controller.addDrink(data);
    }

    //跳回主頁面
    public void switchToMainPage() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("uid", userId);
        startActivity(intent);
        this.finish();
    }

    //建立水波球和拖拉條
    private void initViews() {
        my_view = findViewById(R.id.my_view_drink);
        seek_bar = findViewById(R.id.seek_bar);
        add_water_num = findViewById(R.id.edText_addwater);

        seek_bar.setProgress(0);
        my_view.setProgress(totalCC,0,false);
        add_water_num.setText("0");
        accurateProgress = 0f;

        //拖拉條的監聽，讓水波球和文字同步變更
        seek_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //判斷是seek_bar被動了還是add_water_num被動了
                if(progress != (int)accurateProgress){
                    add_water_num.setText((totalCC * progress / 100) + "");
                }else{
                    add_water_num.setText((int)(totalCC * accurateProgress / 100) + "");
                }
                drinkCC = Integer.parseInt(add_water_num.getText().toString());
                my_view.setProgress(totalCC,drinkCC,false);
            }
        });

        //輸入文字的監聽，讓水波球和拖拉條同步變更
        add_water_num.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                drinkCC = Integer.parseInt(add_water_num.getText().toString());
                accurateProgress = (float)drinkCC * 100 / totalCC;
                seek_bar.setProgress((int)accurateProgress);
                return false;
            }
        });

    }

}
