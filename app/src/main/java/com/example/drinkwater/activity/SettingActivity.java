package com.example.drinkwater.activity;

import android.content.Intent;

import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;

import android.text.InputType;

import android.view.View;

import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.drinkwater.R;
import com.example.drinkwater.service.GoogleAuthService;

public class SettingActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener{

    private RadioGroup radioGroup;
    private RadioButton rb_setting;

    private String userId;

    //預設的提醒間隔、時間區段
    /*private int defaultInterval;
    private int defaultStartTime;
    private int defaultEndTime;
    private int[] defaultRemideDay;*/

    private int defaultInterval = 60;
    private int defaultStartTime = 420; //分鐘為單位
    private int defaultEndTime = 1260; //分鐘為單位
    private boolean[] defaultRemideDay = {true,true,true,true,true,true,true};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        //漸層
        ConstraintLayout constraintLayout = findViewById(R.id.background);
        AnimationDrawable animationDrawable = (AnimationDrawable) constraintLayout.getBackground();
        animationDrawable.setEnterFadeDuration(1500);
        animationDrawable.setExitFadeDuration(3000);
        animationDrawable.start();

        //獲取使用者ID
        Intent intent = this.getIntent();
        userId = intent.getStringExtra("uid");

        //底部選單設置
        radioGroup = findViewById(R.id.radioGroup_menu);
        radioGroup.setOnCheckedChangeListener(this);
        rb_setting = findViewById(R.id.rabtn_setting);
        rb_setting.setChecked(true);

    }

    // 派發事件
    public void eventListener(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btn_userInfo: //個資檢視、修改
                intent = new Intent(this, InfoModificationActivity.class);
                intent.putExtra("uid", userId);
                startActivity(intent);
                break;
            case R.id.btn_interval: //提醒的間隔時間設置
                intervalDialog();
                break;
            case R.id.btn_period: //提醒的時段設置
                periodDialog();
                break;
            case R.id.btn_feedback: //意見回饋
                intent=new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:c108118218@nkust.edu.tw"));
                intent.putExtra(Intent.EXTRA_SUBJECT, "喝喝哈兮使用者回饋");
                startActivity(intent);
                break;
            case R.id.btn_about: //關於
                aobutUsDialog();
                break;
            case R.id.btn_logout: //登出
                GoogleAuthService.signOut();
                intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                this.finish();
                break;
        }
    }

    //輸入間隔提醒時間的彈跳視窗
    private AlertDialog intervalDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(SettingActivity.this);
        View view = getLayoutInflater().inflate(R.layout.dialog_interval,null);
        builder.setView(view);

        //取得須讀取輸入值的元件並給定預設值
        Switch sw_intervalSet = view.findViewById(R.id.sw_intervalSet);
        EditText interval_time = view.findViewById(R.id.interval_time);
        sw_intervalSet.setChecked(defaultInterval != 0); //間隔時間>0才提醒
        interval_time.setText(defaultInterval+"");

        //根據Switch調整EditText狀態(唯讀/輸入)
        sw_intervalSet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    interval_time.setInputType(InputType.TYPE_CLASS_NUMBER);
                }else{
                    interval_time.setInputType(InputType.TYPE_NULL);
                    interval_time.setText(null);
                }
            }
        });

        builder.setPositiveButton(R.string.ok,((dialog, which) -> {}));
        builder.setNegativeButton(R.string.cancel,((dialog, which) -> {}));
        AlertDialog dialog = builder.create();
        dialog.show();

        //確定調整間隔時間
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener((v -> {
            if(sw_intervalSet.isChecked()){
                defaultInterval = Integer.parseInt(interval_time.getText().toString());
            }else{
                defaultInterval = 0;
            }
            dialog.dismiss();
        }));

        //取消調整間隔時間
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener((v -> {
            dialog.dismiss();
        }));

        return dialog;
    }

    //輸入提醒時間區間的彈跳視窗
    private AlertDialog periodDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SettingActivity.this);
        View view = getLayoutInflater().inflate(R.layout.dialog_period,null);
        builder.setView(view);

        //取得須讀取輸入值的元件
        TimePicker begin_timepicker = view.findViewById(R.id.begin_timepicker);
        TimePicker end_timepicker = view.findViewById(R.id.end_timepicker);
        begin_timepicker.setIs24HourView(true);
        end_timepicker.setIs24HourView(true);
        ToggleButton[] tgBtn = new ToggleButton[7];
        for(int i = 0; i < 7; i ++){
            String rId = "tgBtn_" + i;
            tgBtn[i] = view.findViewById(getResources().getIdentifier(rId,"id",getPackageName()));
        }
        //給定預設值
        begin_timepicker.setHour(defaultStartTime/60);
        begin_timepicker.setMinute(defaultStartTime%60);
        end_timepicker.setHour(defaultEndTime/60);
        end_timepicker.setMinute(defaultEndTime%60);
        for(int i = 0; i < 7 ; i ++){
            tgBtn[i].setChecked(defaultRemideDay[i]);
        }

        builder.setPositiveButton(R.string.ok,((dialog, which) -> {}));
        builder.setNegativeButton(R.string.cancel,((dialog, which) -> {}));
        AlertDialog dialog = builder.create();
        dialog.show();

        //確定調整提醒時段
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener((v -> {
            //判斷所選的起始時間是否大於結束時間且間隔大於設定的提醒間隔時間
            int tempStartTime = begin_timepicker.getHour() * 60 + begin_timepicker.getMinute();
            int tempEndTime = end_timepicker.getHour() * 60 + end_timepicker.getMinute();
            if(tempStartTime < tempEndTime){
                if((tempEndTime - tempStartTime) > defaultInterval){
                    //在以下這些日子裡及時間段提醒
                    for(int i = 0; i < 7 ; i ++){
                        defaultRemideDay[i] = tgBtn[i].isChecked();
                    }
                    defaultStartTime = begin_timepicker.getHour() * 60 + begin_timepicker.getMinute();
                    defaultEndTime = end_timepicker.getHour() * 60 + end_timepicker.getMinute();
                    dialog.dismiss();
                }else{
                    Toast.makeText(getApplication(),"時間段不可小於提醒間隔時間!",Toast.LENGTH_LONG).show();
                }
            }else{
                Toast.makeText(getApplication(),"請選擇正確時間段!",Toast.LENGTH_LONG).show();
            }
        }));

        //取消調整提醒時段
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener((v -> {
            dialog.dismiss();
        }));

        return dialog;
    }

    //關於我們的彈跳視窗
    private AlertDialog aobutUsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SettingActivity.this);
        View view = getLayoutInflater().inflate(R.layout.dialog_aboutus,null);
        builder.setView(view);
        AlertDialog dialog = builder.create();
        dialog.show();
        return dialog;
    }

    //底部選單監聽
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId){
            case R.id.rabtn_home:
                switchToHome();
                break;
            case R.id.rabtn_statistics:
                switchToStatistics();
                break;
        }
    }

    //切回主頁面
    private void switchToHome(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(0, 0);
        this.finish();
    }

    //切到設定
    private void switchToStatistics(){
        Intent intent = new Intent(this, StatisticsActivity.class);
        intent.putExtra("uid", userId);
        startActivity(intent);
        overridePendingTransition(0, 0);
        this.finish();
    }

    //禁用返回鍵
    @Override
    public void onBackPressed() {}
}
