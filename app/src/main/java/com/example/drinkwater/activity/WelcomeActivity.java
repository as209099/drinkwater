package com.example.drinkwater.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.example.drinkwater.R;
import com.example.drinkwater.service.GoogleAuthService;
import com.google.firebase.auth.FirebaseUser;

public class WelcomeActivity extends AppCompatActivity {

    private FirebaseUser mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        // 取得使用者資訊
        mUser = GoogleAuthService.getFirebaseUser();


//        switchToPage(MainActivity.class);
//         檢查使用者是否有登入過
        // 讓畫面延遲兩秒
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mUser != null) {
                    switchToPage(MainActivity.class);
                } else {
                    switchToPage(LoginActivity.class);
                }
            }
        }, 1000);

    }

    private void switchToPage(Class activity) {
        Intent intent = new Intent(getApplication(), activity);
        if (mUser != null) {
            intent.putExtra("uid", mUser.getUid());
        }
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        this.finish();
    }

}
