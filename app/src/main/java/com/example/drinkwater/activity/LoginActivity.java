package com.example.drinkwater.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.example.drinkwater.R;
import com.example.drinkwater.service.GoogleAuthService;
import com.example.drinkwater.controller.LoginActivityController;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;

import java.util.Dictionary;
import java.util.Hashtable;

public class LoginActivity extends AppCompatActivity {

    private LoginActivityController controller;
    private GoogleSignInClient mGoogleSignInClient;
    private FirebaseUser mUser;
    private String id;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // 初始化 Controller
        controller = new LoginActivityController();

        // 初始化 Google 登入選項
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("523840786625-7nol22hcgqhlro9df6bi0gilgqrec2oc.apps.googleusercontent.com")
                .requestEmail()
                .build();
        Log.d("My", "GSO Success");
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }


    // Google 登入完後與 Firebase 帳號管理系統詢問是否有該帳號
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("My", "On Activity Result");
        if (requestCode == 200) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // 取得使用者登入資訊
                GoogleSignInAccount account = task.getResult(ApiException.class);
                // 取得使用者登入憑證，用來跟 Firebase Auth 認證
                GoogleAuthService.signInWithCredential(account.getIdToken());

                // 等待使用者登入成功，在開始執行剩下的程序
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        //模擬載入成功後 //等待五秒鐘後
// 取得 FirebaseUser
                        mUser = GoogleAuthService.getFirebaseUser();
                        Log.d("My", "Trying to get User");

                        if (mUser != null) {
                            Log.d("My", "Get User Success");
                            // 取得使用者 uid
                            id = mUser.getUid();

                            // 確認使用者個人資料是否創立
                            Dictionary dict = new Hashtable();
                            dict.put("uid", id);
                            Dictionary result = controller.checkUserExist(dict);
                            Boolean userExist = (Boolean) result.get("userExist");

                            Log.d("My", "Switch Layout");
                            // 檢查使用者是否有有建立個人資料
                            if (userExist) {
                                // 切換主畫面
                                switchToInfoPage(MainActivity.class);
                            }else{
                                // 切換主畫面
                                switchToInfoPage(InfoEstablishActivity.class);
                            }
                        }
                    }
                }, 2000);
                Log.d("My", "Token: " + account.getIdToken());
            } catch (ApiException e) {
                Log.w("INFO", "Google sign in failed", e);
            }
        }
    }


    // 派發事件
    public void eventListener(View view) {
        if (view.getId() == R.id.btn_login) {
            signIn();
        }
    }

    // 登入
    private void signIn() {
        if(mUser == null){

            Log.d("My", "Start Sign in");
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, 200);
        }
    }

    // 切換畫面
    private void switchToInfoPage(Class page) {
        Intent intent = new Intent(this, page);
        intent.putExtra("uid", id);
        startActivity(intent);
        // 淡入淡出的效果
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        this.finish();
    }

    @Override
    public void onBackPressed() {
    } //禁用返回鍵
}
