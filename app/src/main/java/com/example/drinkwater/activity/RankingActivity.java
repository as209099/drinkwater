package com.example.drinkwater.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.drinkwater.R;
import com.example.drinkwater.controller.MainActivityController;

import java.util.Calendar;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Map;

public class RankingActivity extends AppCompatActivity {

    private MainActivityController controller;
    private String userId;

    private TextView rank_name;
    private TextView rank_total;
    private TextView user_total_drink;
    private TextView ranking_date;

    private String date_range;

    private int userNum = 10;

    Calendar calendar = Calendar.getInstance();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        //獲取使用者ID
        Intent intent = this.getIntent();
        userId = intent.getStringExtra("uid");

        // 初始化 MainActivityController
        this.controller = new MainActivityController();

        Map<String, Integer> rankingData = (Map<String, Integer>) controller.getRankingResult().get("ranking");
        int strId;
        int i = 1;
        for (Map.Entry<String, Integer> en : rankingData.entrySet()) {
            //寫入使用者名稱
            strId =getApplicationContext().getResources().getIdentifier("tv_rank_name_" + (i),"id",getPackageName());
            rank_name = findViewById(strId);
            rank_name.setText(en.getKey());

            //寫入使用者總飲用水量
            strId =getApplicationContext().getResources().getIdentifier("tv_rank_total_" + (i),"id",getPackageName());
            rank_total = findViewById(strId);
            rank_total.setText(en.getValue()+"");

            if(i > 10) break;
            else i ++;

        }

        Dictionary dict_uid = new Hashtable();
        dict_uid.put("uid", userId);
        ranking_date = findViewById(R.id.tv_ranking_date);
        getWeekStartEnd();

        date_range = (String) DateFormat.format("yyyy-MM-dd", calendar.getTime()) + " ~ ";
        dict_uid.put("start", (String) DateFormat.format("yyyyMMdd", calendar.getTime()));

        calendar.add(Calendar.DAY_OF_MONTH, 6);
        date_range += (String) DateFormat.format("yyyy-MM-dd", calendar.getTime());
        dict_uid.put("end", (String) DateFormat.format("yyyyMMdd", calendar.getTime()));

        Dictionary selectedDrinkCC = controller.getTotalWater(dict_uid);
        user_total_drink = findViewById(R.id.tv_user_total_drink);
        user_total_drink.setText(selectedDrinkCC.get("total").toString());

        ranking_date.setText(date_range);

    }

    // 派發事件
    public void eventListener(View view) {
        switch (view.getId()){
            case R.id.imgBtn_share:
                System.out.println("Print Output : Sharing!");
                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "喝喝哈兮");
                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, "快看! 我這周在喝喝哈兮喝了 " + user_total_drink.getText().toString() + "cc 的水!");
                startActivity(shareIntent);
                break;
        }
    }

    //獲取該周起始日期
    private void getWeekStartEnd(){
        String temDay;
        for(int i = 0 ; i < 7 ; i ++){
            temDay = (String) DateFormat.format("EEE", calendar.getTime());
            if(!temDay.equals("週一")){
                calendar.add(Calendar.DAY_OF_MONTH, -1);
            }
        }
    }
}
