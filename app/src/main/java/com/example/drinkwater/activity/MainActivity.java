package com.example.drinkwater.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.DatePickerDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.drinkwater.R;
import com.example.drinkwater.controller.MainActivityController;
import com.example.drinkwater.ui_components.MyView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.Calendar;
import java.util.Dictionary;
import java.util.Hashtable;

public class MainActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {

    private MainActivityController controller;
    private MyView my_view;
    private int totalCC;
    private int drinkCC;
    private TextView tv_totalCC;
    private TextView tv_drinkCC;
    private TextView tv_percentageCC;
    private TextView tv_chosenDay;
    private Button btn_drink;
    private RadioGroup radioGroup;
    private RadioButton rb_home;
    private String userId;
    private Calendar calendar_today = Calendar.getInstance();
    private Calendar calendar_chosenDay = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId  = "default_notification_channel_id";
            String channelName = "default_notification_channel_name";
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        }

        //漸層部分程式碼
        ConstraintLayout constraintLayout = findViewById(R.id.background);
        AnimationDrawable animationDrawable = (AnimationDrawable) constraintLayout.getBackground();
        animationDrawable.setEnterFadeDuration(1500);
        animationDrawable.setExitFadeDuration(3000);
        animationDrawable.start();

        userId = "SUGaq90IGKbHqhXDzaE2EfzN02P2";

        // 初始化 MainActivityController
        this.controller = new MainActivityController();

        //畫面顯示文字設置
        tv_totalCC = findViewById(R.id.tv_totalCC);
        tv_drinkCC = findViewById(R.id.tv_drinkCC);
        tv_percentageCC = findViewById(R.id.tv_percentageCC);
        tv_chosenDay = findViewById(R.id.tv_chosenDay);
        btn_drink = findViewById(R.id.btn_drink);

        changeShowData();

        // 取得 Firebase Cloud Messaging Token
        initCloudMessaging();

        //底部選單設置
        radioGroup = findViewById(R.id.radioGroup_menu);
        radioGroup.setOnCheckedChangeListener(this);
        rb_home = findViewById(R.id.rabtn_home);
        rb_home.setChecked(true); //預設在主頁

    }

    // 取得 Firebase Cloud Messaging Token
    private void initCloudMessaging() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w("INFO", "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult();

                        // Log and toast
//                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.d("My", "My FCM token: " + token);
//                        Toast.makeText(MainActivity.this, token, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    // 處理前端介面所有的事件
    public void eventListener(View view) {
        switch (view.getId()) {
            case R.id.btn_drink: //喝水按鈕
                addDrink();
                break;
            case R.id.tv_chosenDay: //選擇顯示日期
                datePicker(view);
                break;
            case R.id.btn_next: //下一天
                calendar_chosenDay.add(Calendar.DAY_OF_MONTH, 1);
                checkIfToday();
                break;
            case R.id.btn_previous: //前一天
                calendar_chosenDay.add(Calendar.DAY_OF_MONTH, -1);
                checkIfToday();
                break;
            case R.id.fab_rank: //排行
                Intent intent = new Intent(this,RankingActivity.class);
                intent.putExtra("uid", userId);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    // 改變頁面上所有數據資料
    private void changeShowData(){

        // 抓取該日飲用量與總飲用量
        String selectedDate = (String) DateFormat.format("yyyyMMdd", calendar_chosenDay.getTime());
        Dictionary dict_uid = new Hashtable();
        dict_uid.put("uid", userId);
        dict_uid.put("start", selectedDate);
        dict_uid.put("end", selectedDate);
        Dictionary selectedDrinkCC = controller.getTotalWater(dict_uid);
        Dictionary selectedTotalCC = controller.getDailyDrinkAmount(dict_uid);

        // 寫入 totalCC 與 drinkCC
        totalCC = (int)selectedTotalCC.get("dailyDrinkAmount");
        drinkCC = (int)selectedDrinkCC.get("total");

        // 更新顯示內容
        tv_totalCC.setText("目　標\n" + totalCC + "cc");
        tv_drinkCC.setText("已飲用\n" + drinkCC + "cc");
        tv_percentageCC.setText("已達成\n" + (drinkCC * 100 / totalCC) + "%");
        initViews();

    }

    //選擇日期的Dialog
    public void datePicker(View view) {
        int year = calendar_today.get(Calendar.YEAR);
        int month = calendar_today.get(Calendar.MONTH);
        int day = calendar_today.get(Calendar.DAY_OF_MONTH);

        new DatePickerDialog(view.getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                calendar_chosenDay.set(y, m, d);
                checkIfToday();
            }
        }, year, month, day).show();
    }

    //檢查選擇日期是否為今天 => 決定是否開啟喝水按鈕
    public void checkIfToday() {
        String today = (String) DateFormat.format("yyyy-MM-dd", calendar_today.getTime());
        String chosenDay = (String) DateFormat.format("yyyy-MM-dd", calendar_chosenDay.getTime());

        if (chosenDay.equals(today)) {
            tv_chosenDay.setText(R.string.today);
            btn_drink.setEnabled(true);
        } else {
            tv_chosenDay.setText(chosenDay);
            btn_drink.setEnabled(false);
        }

        changeShowData();
    }

    //新增喝水量按鈕
    private void addDrink() {
        Intent intent = new Intent(this, DrinkWaterActivity.class);
        intent.putExtra("uid", userId);
        intent.putExtra("drinkcc", String.valueOf(drinkCC));
        intent.putExtra("totalcc", String.valueOf(totalCC));

        startActivity(intent);
    }

    //建立水波球
    private void initViews() {
        my_view = findViewById(R.id.my_view_main);
        my_view.setProgress(totalCC,drinkCC,true);
    }

    //底部選單監聽
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rabtn_statistics: //統計頁面
                radioSwitchPage(StatisticsActivity.class);
                break;
            case R.id.rabtn_setting: //設定頁面
                radioSwitchPage(SettingActivity.class);
                break;
        }
    }

    //Radio切換畫面
    private void radioSwitchPage(Class nextClass) {
        Intent intent = new Intent(this, nextClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("uid", userId);
        startActivity(intent);
    }

    //禁用返回鍵
    @Override
    public void onBackPressed() {
    }
}
