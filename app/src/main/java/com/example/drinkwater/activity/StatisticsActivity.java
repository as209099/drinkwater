package com.example.drinkwater.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.drinkwater.R;
import com.example.drinkwater.controller.MainActivityController;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

public class StatisticsActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener{

    private MainActivityController controller;

    private RadioGroup radioGroup;
    private RadioButton rb_statistics;

    private BarChart bar_chart;

    // True 以日檢視 False 以周檢視
    private boolean showType = true;

    private Calendar calendar_today = Calendar.getInstance();
    private Calendar calendar_chosenDay = Calendar.getInstance();
    private Calendar calendar_startDay = Calendar.getInstance();

    private TextView tv_chosenDay;

    private TextView tv_averageDrink;
    private TextView tv_startDrink;
    private TextView tv_lastDrink;
    private TextView tv_longestInterval;
    private TextView tv_averageInterval;
    private TextView tv_shortestInterval;

    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        //漸層
        ConstraintLayout constraintLayout = findViewById(R.id.background);
        AnimationDrawable animationDrawable = (AnimationDrawable) constraintLayout.getBackground();
        animationDrawable.setEnterFadeDuration(1500);
        animationDrawable.setExitFadeDuration(3000);
        animationDrawable.start();

        //獲取使用者ID
        Intent intent = this.getIntent();
        userId = intent.getStringExtra("uid");

        // 初始化 MainActivityController
        this.controller = new MainActivityController();

        //底部選單設置
        radioGroup = findViewById(R.id.radioGroup_menu);
        radioGroup.setOnCheckedChangeListener(this);
        rb_statistics = findViewById(R.id.rabtn_statistics);
        rb_statistics.setChecked(true);

        //尋找控制元件
        bar_chart = findViewById(R.id.chart_bar);
        tv_chosenDay = findViewById(R.id.tv_chosenDay);

        tv_averageDrink = findViewById(R.id.tv_averageDrink);
        tv_startDrink = findViewById(R.id.tv_startDrink);
        tv_lastDrink = findViewById(R.id.tv_lastDrink);
        tv_longestInterval = findViewById(R.id.tv_longestInterval);
        tv_averageInterval = findViewById(R.id.tv_averageInterval);
        tv_shortestInterval = findViewById(R.id.tv_shortestInterval);

        showDayData();
    }

    //顯示該日每次資料
    private void showDayData(){

        //Y軸資料
        String selectedDate = (String) DateFormat.format("yyyyMMdd", calendar_chosenDay.getTime());
        List<BarEntry> barDayData = new ArrayList<>();
        Dictionary dict_uid = new Hashtable();
        dict_uid.put("uid", userId);
        dict_uid.put("start", selectedDate);
        dict_uid.put("end", selectedDate);

        System.out.println("Print Output : " + controller.getAllDrinkRecord(dict_uid));
        List<Integer> drinkRecords = (List<Integer>)(controller.getAllDrinkRecord(dict_uid)).get("drinkRecords");
        for(int i = 0; i < drinkRecords.size(); i++){
            barDayData.add(new BarEntry(i,drinkRecords.get(i)));
        }

        List<String> chartLabels = new ArrayList<String>();
        List<String> timeRecords = (List<String>)(controller.getAllDrinkRecord(dict_uid)).get("timeRecords");
        for(int i = 0; i < timeRecords.size(); i++){
            chartLabels.add(timeRecords.get(i).substring(0,2) + ":" +timeRecords.get(i).substring(2,4));
        }

        bar_chart.getXAxis().setValueFormatter(new com.github.mikephil.charting.formatter.IndexAxisValueFormatter(chartLabels)); //字串轉成BarEntry

        //下方標籤設定
        String chosenDay = (String) DateFormat.format("yyyy-MM-dd", calendar_chosenDay.getTime());
        BarDataSet setDayBarChart = new BarDataSet(barDayData, chosenDay+"每次飲水量");
        setDayBarChart.setColor(getResources().getColor(R.color.blue_normal));
        setDayBarChart.setValueTextSize(15f);

        //資料傳入圖表
        BarData data = new BarData(setDayBarChart);
        bar_chart.setData(data);//設定資料
        bar_chart.invalidate();//重新整理
        bar_chart.animateY(1500);
        barChartStyleSet();
        showDayStatistics();
    }

    //顯示日的統計資料
    private void showDayStatistics(){
        Dictionary dict_uid = new Hashtable();
        dict_uid.put("uid", userId);

        System.out.println("Print Output : " + controller.getDrinkAnalyze(dict_uid));

        Dictionary drinkAnalyze = (Dictionary)(controller.getDrinkAnalyze(dict_uid)).get("dailyAnalyze");

        System.out.println("Print Output : " + drinkAnalyze);

        int averageDrink = Integer.parseInt(drinkAnalyze.get("averageDrinkTime").toString());
        int startDrinkHour = Integer.parseInt(drinkAnalyze.get("startDrink").toString()) / 60 / 60;
        int startDrinkMin = Integer.parseInt(drinkAnalyze.get("startDrink").toString()) / 60 % 60;
        int endDrinkHour = Integer.parseInt(drinkAnalyze.get("endDrink").toString()) / 60 / 60;
        int endDrinkMin = Integer.parseInt(drinkAnalyze.get("endDrink").toString()) / 60 % 60;
        int longInterDrinkHour = Integer.parseInt(drinkAnalyze.get("longestInterval").toString()) / 60 / 60;
        int longInterDrinkMin = Integer.parseInt(drinkAnalyze.get("longestInterval").toString()) / 60 % 60;
        int shortInterDrinkHour = Integer.parseInt(drinkAnalyze.get("shortestInterval").toString()) / 60 / 60;
        int shortInterDrinkMin = Integer.parseInt(drinkAnalyze.get("shortestInterval").toString()) / 60 % 60;

        tv_averageDrink.setText(averageDrink / 60  + "分鐘\n平均飲水");
        tv_startDrink.setText(startDrinkHour + ":" + startDrinkMin + "\n開始飲用");
        tv_lastDrink.setText(endDrinkHour + ":" + endDrinkMin +"\n最後飲用");
        tv_longestInterval.setText(longInterDrinkHour + "時" + longInterDrinkMin + "分\n最長間隔");
        tv_averageInterval.setText("");
        tv_shortestInterval.setText(shortInterDrinkHour + "時" + shortInterDrinkMin + "分\n最短間隔");
    }

    //顯示該週每日資料
    private void showWeekData(){

        //Y軸資料
        String startDay = (String) DateFormat.format("yyyy-MM-dd", calendar_startDay.getTime());
        List<BarEntry> barWeekData = new ArrayList<>();

        Dictionary dict_uid = new Hashtable();
        dict_uid.put("uid", userId);
        for(int i = 0; i < 7; i++){
            String selectedDate = (String) DateFormat.format("yyyyMMdd", calendar_chosenDay.getTime());
            dict_uid.put("start", selectedDate);
            dict_uid.put("end", selectedDate);
            Dictionary selectedDrinkCC = controller.getTotalWater(dict_uid);
            barWeekData.add(new BarEntry(i,(int)selectedDrinkCC.get("total")));
            calendar_startDay.add(Calendar.DAY_OF_MONTH, 1);
        }
        // 減掉迴圈多加的部分
        calendar_startDay.add(Calendar.DAY_OF_MONTH, -1);
        String endDay = (String) DateFormat.format("yyyy-MM-dd", calendar_startDay.getTime());
        calendar_startDay.add(Calendar.DAY_OF_MONTH, -6);

        //X軸資料
        List<String> chartLabels = new ArrayList<>();
        chartLabels.add("週一");
        chartLabels.add("週二");
        chartLabels.add("週三");
        chartLabels.add("週四");
        chartLabels.add("週五");
        chartLabels.add("週六");
        chartLabels.add("週日");
        bar_chart.getXAxis().setValueFormatter(new com.github.mikephil.charting.formatter.IndexAxisValueFormatter(chartLabels));

        //下方標籤設定
        BarDataSet setDayBarChart = new BarDataSet(barWeekData, startDay + " ~ " + endDay +"每日飲水量");
        setDayBarChart.setColor(getResources().getColor(R.color.blue_normal));
        setDayBarChart.setValueTextSize(10f);

        //資料傳入圖表
        BarData data = new BarData(setDayBarChart);
        bar_chart.setData(data);//設定資料
        bar_chart.invalidate();//重新整理
        bar_chart.animateY(1500);
        barChartStyleSet();
    }

    //顯示周的統計資料
    private void showWeekStatistics(){
        Dictionary dict_uid = new Hashtable();
        dict_uid.put("uid", userId);
        Dictionary drinkAnalyze = (Dictionary)(controller.getDrinkAnalyze(dict_uid)).get("weeklyAnalyze");

        int averageDrink = Integer.parseInt(drinkAnalyze.get("averageDailyDrinkTime").toString());
        int startDrinkHour = Integer.parseInt(drinkAnalyze.get("averageStartDrink").toString()) / 60 / 60;
        int startDrinkMin = Integer.parseInt(drinkAnalyze.get("averageStartDrink").toString()) / 60 % 60;
        int endDrinkHour = Integer.parseInt(drinkAnalyze.get("averageEndDrink").toString()) / 60 / 60;
        int endDrinkMin = Integer.parseInt(drinkAnalyze.get("averageEndDrink").toString()) / 60 % 60;
        int interDrinkHour = Integer.parseInt(drinkAnalyze.get("averageDrinkInterval").toString()) / 60 / 60;
        int interDrinkMin = Integer.parseInt(drinkAnalyze.get("averageDrinkInterval").toString()) / 60 % 60;

        tv_averageDrink.setText(averageDrink + "次\n平均飲水");
        tv_startDrink.setText(startDrinkHour+ ":" + startDrinkMin + "\n平均開始");
        tv_lastDrink.setText(endDrinkHour + ":" + endDrinkMin + "\n平均最晚");
        tv_longestInterval.setText("");
        tv_averageInterval.setText(interDrinkHour + "時" + interDrinkMin + "分\n平均間隔");
        tv_shortestInterval.setText("");
    }

    //BarChart顯示設定
    private void barChartStyleSet(){



        bar_chart.getDescription().setEnabled(false);
        bar_chart.getAxisRight().setEnabled(false);
        bar_chart.setScaleEnabled(false);
        bar_chart.setVisibleXRange(7,7);


        XAxis xAxis = bar_chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f);
        xAxis.setTextSize(18f);

        YAxis yAxis = bar_chart.getAxisLeft();
        yAxis.setLabelCount(5,false);
        yAxis.setDrawGridLines(false);
        yAxis.setTextSize(20f);

        Legend legend = bar_chart.getLegend();
        legend.setTextSize(20f);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);

    }

    // 派發事件
    public void eventListener(View view) {
        switch (view.getId()){
            case R.id.btn_viewByDay:
                showDayStatistics();
                showType = !showType;
                break;
            case R.id.btn_viewByWeek:
                showWeekStatistics();
                showType = !showType;
                break;
            case R.id.tv_chosenDay:
                datePicker(view);
                break;
            case R.id.btn_next: //下一天
                if(showType){
                    calendar_chosenDay.add(Calendar.DAY_OF_MONTH, 1);
                }else{
                    calendar_chosenDay.add(Calendar.DAY_OF_MONTH, 7);
                }
                checkIfToday();
                break;
            case R.id.btn_previous: //前一天
                if(showType){
                    calendar_chosenDay.add(Calendar.DAY_OF_MONTH, -1);
                }else{
                    calendar_chosenDay.add(Calendar.DAY_OF_MONTH, -7);
                }
                checkIfToday();
                break;
            default:
                break;
        }

        // 判斷現在檢視方式
        if(showType){
            showDayData();
        }else{
            getWeekStartEnd();
            showWeekData();
        }

    }

    //選擇日期的Dialog
    public void datePicker(View view) {
        int year = calendar_today.get(Calendar.YEAR);
        int month = calendar_today.get(Calendar.MONTH);
        int day = calendar_today.get(Calendar.DAY_OF_MONTH);

        new DatePickerDialog(view.getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                calendar_chosenDay.set(year,month,day);
                checkIfToday();
                if(showType){
                    showDayData();
                }else{
                    getWeekStartEnd();
                    showWeekData();
                }
            }
        }, year, month, day).show();
    }

    //檢查選擇日期是否為今天
    public void checkIfToday(){
        String today = (String) DateFormat.format("yyyy-MM-dd", calendar_today.getTime());
        String chosenDay = (String) DateFormat.format("yyyy-MM-dd", calendar_chosenDay.getTime());
        if(chosenDay.equals(today)){
            tv_chosenDay.setText(R.string.today);
        }else{
            tv_chosenDay.setText(chosenDay);
        }
    }

    //獲取該周起始日期
    private void getWeekStartEnd(){
        Calendar calendar_temp = calendar_chosenDay;
        String temDay;
        for(int i = 0 ; i < 7 ; i ++){
            temDay = (String) DateFormat.format("EEE", calendar_temp.getTime());
            if(temDay.equals("週一")){
                calendar_startDay = calendar_temp;
                break;
            }else{
                calendar_temp.add(Calendar.DAY_OF_MONTH, -1);
            }
        }
    }

    //底部選單監聽
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId){
            case R.id.rabtn_home:
                switchToHome();
                break;
            case R.id.rabtn_setting:
                switchToSetting();
                break;
        }
    }

    //切回主頁面
    private void switchToHome(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(0, 0);
        this.finish();
    }

    //切到設定
    private void switchToSetting(){
        Intent intent = new Intent(this, SettingActivity.class);
        intent.putExtra("uid", userId);
        startActivity(intent);
        overridePendingTransition(0, 0);
        this.finish();
    }

    //禁用返回鍵
    @Override
    public void onBackPressed() {}
}
