package com.example.drinkwater.service;

import android.text.format.DateFormat;
import android.util.Log;

import com.example.drinkwater.model.Record;
import com.example.drinkwater.model.User;
import com.example.drinkwater.repository.UserRepository;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class RankingService implements Runnable {

    private static final String TAG = "My";
    public static int sum = 0;
    private volatile Dictionary result;

    private Calendar calendar_chosenDay = Calendar.getInstance();
    private Calendar calendar_startDay = Calendar.getInstance();

    @Override
    public void run() {
        synchronized (this) {
            Dictionary test = new Hashtable();
            getWeekStartEnd();

            // 取得當天的日期
            Date now = new Date();
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd");

            // 取得七天前的日期
//            Calendar cal = Calendar.getInstance();
//            cal.setTime(now);
//            cal.add(Calendar.DATE, -7);
            String start = sdfDate.format(calendar_startDay.getTime());
            test.put("date", start);
            Dictionary result = UserRepository.getDrinkCollection(test);
            Task<QuerySnapshot> task = (Task) result.get("task");
            this.result = calculateRanking(task);

            notify();
        }
    }
    //獲取該周起始日期
    private void getWeekStartEnd(){
        Calendar calendar_temp = calendar_chosenDay;
        String temDay;
        for(int i = 0 ; i < 7 ; i ++){
            temDay = (String) DateFormat.format("EEE", calendar_temp.getTime());
            if(temDay.equals("週一")){
                calendar_startDay = calendar_temp;
                break;
            }else{
                calendar_temp.add(Calendar.DAY_OF_MONTH, -1);
            }
        }
    }

    public Dictionary getResult(){
        return result;
    }

    private Dictionary calculateRanking(Task<QuerySnapshot> task) {
        Dictionary result = new Hashtable();

        List<Integer> drinkVolume = new ArrayList<Integer>();
        List<String> name = new ArrayList<String>();
        HashMap<String, Integer> hm = new HashMap<String, Integer>();

        HashMap<String, String> uidToName = new HashMap<>();


        for (QueryDocumentSnapshot document : task.getResult()) {
            Record record = document.toObject(Record.class);
            String uid = record.getUid();
            if (uidToName.containsKey(uid)) {
                Integer drink = hm.get(uidToName.get(uid)) + record.getDrinkVolume();
                hm.put(uidToName.get(uid), drink);

//                Integer pos = name.indexOf(record.getUid());
//                Integer drink = drinkVolume.get(pos) + record.getDrinkVolume();
//                drinkVolume.set(pos, drink);
            } else {
                Dictionary dict_uid = new Hashtable();
                dict_uid.put("uid", uid);
                User user = (User) UserRepository.getUser(dict_uid).get("user");
                uidToName.put(uid, user.getName());
                hm.put(user.getName(), record.getDrinkVolume());

                name.add(record.getUid());
//                drinkVolume.add(record.getDrinkVolume());
            }
//            hm.put("midOilIsStupid", 10000);
//            Log.d(TAG, String.format("calculateRanking: %s %d", record.getUid(), record.getDrinkVolume()));
        }
        Map<String, Integer> hm1 = sortByValue(hm);
//        for (Map.Entry<String, Integer> en : hm1.entrySet()) {
//            Log.d(TAG, "Key = " + en.getKey() +", Value = " + en.getValue());
//        }
//        for (String s : name) {
//            Log.d(TAG, String.format("calculateRanking: %s %d", s, drinkVolume.get(name.indexOf(s))));
//        }
        result.put("ranking", hm1);
        return result;
    }

    // function to sort hashmap by values
    public static HashMap<String, Integer> sortByValue(HashMap<String, Integer> hm){
        // Create a list from elements of HashMap
        List<Map.Entry<String, Integer> > list =
                new LinkedList<Map.Entry<String, Integer> >(hm.entrySet());

        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<String, Integer> >() {
            public int compare(Map.Entry<String, Integer> o2,
                               Map.Entry<String, Integer> o1)
            {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // put data from sorted list to hashmap
        HashMap<String, Integer> temp = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }

}