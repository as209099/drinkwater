package com.example.drinkwater.service;

import android.util.Log;

import com.example.drinkwater.model.Record;
import com.example.drinkwater.model.User;
import com.example.drinkwater.repository.UserRepository;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class UserService {

//    private UserRepository userRepo;

    public UserService() {
        // 初始化UserRepo
//        this.userRepo = new UserRepository();
    }

    static {

    }

    // 處理喝水事件
    public static Dictionary drinkWater(Dictionary dict) {
        // 取得喝水時間
        Date now = new Date();
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdfTime = new SimpleDateFormat("HHmmss");
        dict.put("date", sdfDate.format(now));
        dict.put("time", sdfTime.format(now));
        dict.put("timestamp", new Timestamp(new Date()));

        // 建立 record 物件，準備傳進 UserRepo。
        Record record = new Record(dict);
        Dictionary data = new Hashtable();
        data.put("record", record);

        // 把資料傳送到 UserRepo
        Dictionary result = UserRepository.drinkWater(data);

        // 回傳結果
        return result;
    }

    // 取得某日總喝水量
    public static Dictionary getTotalWater(Dictionary dict) {
        // 把資料傳送到 UserRepo
        Dictionary data = UserRepository.getDrinkRecord(dict);

        int totalVolume = 0;
        Task<QuerySnapshot> task = (Task) data.get("task");
        // 開始計算今日總喝水量
        for (QueryDocumentSnapshot document : task.getResult()) {
            Map<String, Object> documentMap = document.getData();
            int drinkVolume = Integer.parseInt(documentMap.get("drinkVolume").toString());
            totalVolume += drinkVolume;
        }

        // 回傳數值
        Dictionary result = new Hashtable();
        result.put("total", totalVolume);
        // 回傳結果
        return result;
    }

    // 取得某日全部的喝水資料
    public static Dictionary getAllDrinkRecord(Dictionary dict) {
        // 把資料傳送到 UserRepo
        Dictionary data = UserRepository.getDrinkRecord(dict);
        List<Integer> drinkRecords = new ArrayList<>();
        List<String> timeRecords = new ArrayList<>();
        Task<QuerySnapshot> task = (Task) data.get("task");
        // 開始計算今日總喝水量
        for (QueryDocumentSnapshot document : task.getResult()) {
            Map<String, Object> documentMap = document.getData();
            int drinkVolume = Integer.parseInt(documentMap.get("drinkVolume").toString());
            drinkRecords.add(drinkVolume);
            timeRecords.add(documentMap.get("time").toString());

//            totalVolume += drinkVolume;
        }
        Log.d("My", "getAllDrinkRecord: " + drinkRecords);
        Log.d("My", "getAllDrinkRecord: " + timeRecords);

        // 回傳數值
        Dictionary result = new Hashtable();
        result.put("drinkRecords", drinkRecords);
        result.put("timeRecords", timeRecords);
        // 回傳結果
        return result;
    }

    // 新增使用者
    public static Dictionary addUser(Dictionary dict) {
        // 建立 User 物件
        User user = new User(dict);
        Dictionary data = new Hashtable();
        data.put("data", user);

        // 把資料傳送到 UserRepo
        Dictionary result = UserRepository.addUser(data);

        // 回傳結果
        return result;
    }

    // input:
    //      uid: "asdfhih1fueiorjgkj30g"
    // output:
    //      "result": "success"
    //      "userExist" : true
    public static Dictionary checkUserExist(Dictionary dict) {
        Dictionary data = UserRepository.getUser(dict);

        Dictionary result = new Hashtable();

        if (data.get("result") == "success") {
            result.put("userExist", true);
        } else {
            result.put("userExist", false);
        }

        // 回傳結果
        return result;
    }

    // input:
    //      uid: "asdfhih1fueiorjgkj30g"
    // output:
    //      "result": "success"
    //      "user" : User Object
    // 取得建議喝水量
    public static Dictionary getDailyDrinkAmount(Dictionary dict) {
        // 從 UserRepo裡面讀取使用者資料
        Dictionary data = UserRepository.getUser(dict);

        Dictionary result = new Hashtable();

        if (data.get("result") == "success") {
            // 建立 User 物件
            User user = (User) data.get("user");
            // 取得建議喝水量
            int dailyDrinkAmount = user.getDailyDrinkAmount();
            // 將資料包裝，準備回傳
            result.put("dailyDrinkAmount", dailyDrinkAmount);
        } else {
            // 取得失敗就回傳 error
            return data;
        }

        // 回傳結果
        return result;
    }

}