package com.example.drinkwater.service;

import android.util.Log;

import com.example.drinkwater.model.Record;
import com.example.drinkwater.repository.UserRepository;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class DrinkAnalyzeService {

    private final static String TAG = "My";
    // Static 建構子進入點
    static {

    }

    public DrinkAnalyzeService() {
        // do nothing
    }

    public static Dictionary getDrinkAnalyze(Dictionary dict) {
        // 取得當天的日期
        Date now = new Date();
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd");
        String end = sdfDate.format(now);

        // 取得七天前的日期
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DATE, -7);
        String start = sdfDate.format(cal.getTime());

        // 將所需的資料包裝
        dict.put("start", start);
        dict.put("end", end);

        List dailyRecord = new ArrayList();
        List weeklyRecord = new ArrayList();

        // 取得喝水紀錄
        Dictionary drinkRecord = UserRepository.getDrinkRecord(dict);
        Task<QuerySnapshot> task = (Task) drinkRecord.get("task");

        for (QueryDocumentSnapshot document : task.getResult()) {
            Map<String, Object> documentMap = document.getData();
            Record record = document.toObject(Record.class);
            if (record.getDate().equals(end)) {
                dailyRecord.add(record);
            }
            weeklyRecord.add(record);
//            Log.d("My", documentMap.get("drinkVolume").toString());
        }

        Dictionary result = new Hashtable();
        result.put("dailyAnalyze", getDailyAnalyze(dailyRecord));
        result.put("weeklyAnalyze", getWeeklyAnalyze(weeklyRecord));
        return result;
    }

    // 平均幾小時幾分鐘喝一次水，開始喝水時間點與最後一次喝水時間點(就好比起床和睡著時間點)，間格最長和最短的喝水時長。
    private static Dictionary getDailyAnalyze(List<Record> dailyRecord) {
        SimpleDateFormat myFormatter = new SimpleDateFormat("HHmmss");
        myFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        Dictionary result = new Hashtable();
        long averageDrinkTime = 0;
        long startDrink = 0;
        long endDrink = 0;
        long longestInterval = 0;
        long shortestInterval = 0;

//        result.put("")
        if (dailyRecord.size() < 2) {
            Log.d("My", "getDailyAnalyze: 喝水資訊太少，不能進行處理");
            result.put("result", "error");
            return result;
        }else{
            Log.d("My", "getDailyAnalyze: 開始分析日常喝水");
        }

        try {
            // 計算一天內平均間隔多久喝一次水
            Record record1 = (Record) dailyRecord.get(0);
            Record record2 = (Record) dailyRecord.get(dailyRecord.size() - 1);
            Date date1 = myFormatter.parse(record1.getTime());
            Date date2 = myFormatter.parse(record2.getTime());
            long diff1 = (date2.getTime() - date1.getTime()) / (1000);
            averageDrinkTime = diff1 / dailyRecord.size();
            Log.d("My", String.format("averageDrinkTime: %s Seconds per time", +averageDrinkTime));

            startDrink = date1.getTime() % (1000 * 60 * 60 * 24) / (1000);
            endDrink = date2.getTime() % (1000 * 60 * 60 * 24) / (1000);
            // 開始喝水時間點與最後一次喝水時間點
            Log.d("My", String.format("Start Drink Time: %s Seconds", startDrink));
            Log.d("My", String.format("End Drink Time: %s Seconds", endDrink));

            // 間格最長和最短的喝水時長
            for (int i = 0; i < dailyRecord.size() - 1; i++) {
                Record record3 = (Record) dailyRecord.get(i);
                Record record4 = (Record) dailyRecord.get(i + 1);
                Date date3 = myFormatter.parse(record3.getFullTime());
                Date date4 = myFormatter.parse(record4.getFullTime());
                long diff2 = (date4.getTime() - date3.getTime()) / (1000);

                if (i == 0) {
                    longestInterval = diff2;
                } else if (diff2 > longestInterval) {
                    longestInterval = diff2;
                }
                if (i == 0) {
                    shortestInterval = diff2;
                } else if (diff2 < shortestInterval) {
                    shortestInterval = diff2;
                }
//                Log.d("My", String.format("%d Seconds", diff2));

            }
            Log.d("My", String.format("ShortInterval: %d Seconds, LongestInterval: %d Seconds ", shortestInterval, longestInterval));

            result.put("averageDrinkTime", averageDrinkTime);
            result.put("startDrink", startDrink);
            result.put("endDrink", endDrink);
            result.put("longestInterval", longestInterval);
            result.put("shortestInterval", shortestInterval);
            result.put("result", "success");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }


    // 平均一天喝幾次水、平均初始喝水時間點及最晚喝水時間點、平均間隔喝水時長。
    private static Dictionary getWeeklyAnalyze(List<Record> weeklyRecord) {
        SimpleDateFormat myFormatter = new SimpleDateFormat("HHmmss");
        myFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        int averageDailyDrinkTime = 0;
        long averageStartDrink = 0;
        long averageEndDrink = 0;
        long averageDrinkInterval = 0;
        int realDrinkDay = 0;
        Dictionary result = new Hashtable();

        if (weeklyRecord.size() < 2) {
            Log.d("My", "getWeeklyAnalyze: 喝水資訊太少，不能進行處理");
            result.put("result", "error");
            return result;
        }else{
            Log.d("My", "getWeeklyAnalyze: 開始分析周喝水");
        }

        try {
            // 開始尋找喝水的規律
            for (int i = 0; i < weeklyRecord.size() - 1; i++) {
                Record record1 = weeklyRecord.get(i);
                Record record2 = weeklyRecord.get(i + 1);
                Date date1 = myFormatter.parse(record1.getTime());
                Date date2 = myFormatter.parse(record2.getTime());
                // 檢查是否有換日
                if (!record1.getDate().equals(record2.getDate())) {
//                    Log.d("My", "Calc Real Day: " + record1.getDate() + ", " + record2.getDate());
                    realDrinkDay += 1;
                    averageStartDrink += date2.getTime();
//                    Log.d(TAG, "getWeeklyAnalyze: start record volume" + record2.getTime());
//                    Log.d(TAG, "getWeeklyAnalyze: start drink volume" + date2.getTime());
                    averageEndDrink += date1.getTime();
                } else {
                    long diff1 = (date2.getTime() - date1.getTime()) / (1000);
                    averageDrinkInterval += diff1;
                }
            }

            averageDailyDrinkTime = weeklyRecord.size() / realDrinkDay;
            Log.d("My", "Real Drink Day: " + realDrinkDay);
            Log.d("My", "Weekly Average Drink Frequence: " + averageDailyDrinkTime);

            // 最近七天的平均最早以及最晚喝水時間
            averageStartDrink = (averageStartDrink / realDrinkDay) % (1000 * 60 * 60 * 24) / (1000);
            averageEndDrink = (averageEndDrink / realDrinkDay) % (1000 * 60 * 60 * 24) / (1000);
            Log.d("My", String.format("Weekly Start Drink Time: %s Second", averageStartDrink));
            Log.d("My", String.format("Weekly End Drink Time: %s Second", averageEndDrink));

            // 最近七天內的平均喝水間隔
            averageDrinkInterval = averageDrinkInterval / (weeklyRecord.size() - realDrinkDay - 1);
            Log.d("My", String.format("Weekly Drink Interval: %s Seconds", averageDrinkInterval));

            result.put("averageDailyDrinkTime", averageDailyDrinkTime);
            result.put("averageStartDrink", averageStartDrink);
            result.put("averageEndDrink", averageEndDrink);
            result.put("averageDrinkInterval", averageDrinkInterval);
            result.put("result", "success");
        } catch (ParseException e) {
            e.printStackTrace();
            result.put("result", "error");
        } finally {
            return result;
        }
    }
}
